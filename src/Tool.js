function getDateAsUTC(date){
    if(date){
        return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
    }else{
        return date;
    }
}

function getDateUtc(){
    date = new Date();
    return getDateAsUTC(date);
}

function getDateAsUTCDao(date){
    if(date){
        date = new Date(date)
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
    }else{
        return date;
    }
}


module.exports = {
    getDateAsUTC,
    getDateUtc,
    getDateAsUTCDao
}
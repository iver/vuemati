import Config from './config'
import axios from 'axios';

function getIdPerson(){
    var _idPerson = localStorage.getItem('_idPerson')
    if(_idPerson != null && _idPerson != undefined && _idPerson != 'undefined'){
        if(_idPerson.length > 0){
            return _idPerson
        }
    }
    return ''
}

function getIdUser(){
    var _idUser = localStorage.getItem('_idUser')
    if(_idUser != null && _idUser != undefined && _idUser != 'undefined'){
        if(_idUser.length > 0){
            return _idUser
        }
    }
    return ''
}

function getRfcCurrent(){
    var _rfc = localStorage.getItem('_rfc')
    if(_rfc != null && _rfc != undefined && _rfc != 'undefined'){
        if(_rfc.length > 0){
            return _rfc
        }
    }
    return ''
}

function getToken(){    
    var token = localStorage.getItem('token')
    if(token != null && token != undefined && token != 'undefined'){
        if(token.length > 0){
            return token
        }
    }
    return ''
}

function getDataProduct(){    
    var datProduct = localStorage.getItem('dataProduct');
    if(datProduct != null && datProduct != undefined){
       return JSON.parse(datProduct);
    }
    return null;
}

function changeLanguage (localec = 'en'){  
    /*axios.get(`${Config.SERVER}messageBundle?lang=`+ localec).then((response) => {
        console.log("entro 2/2", response)
        setLang(localec);
        storeMessages(response.data);
    });*/
}

function setLang(locale){
    localStorage.setItem('LANG', locale)    
}

function getMessage(code){
    let locale_messages = JSON.parse(localStorage.getItem('messages'));
    if( locale_messages ){
       return locale_messages[ code ];
    }
    else{
       return code;
    }
}

function storeMessages (data){
    localStorage.setItem('messages',JSON.stringify(data));
}


function getRfcGeneric(){
    return 'NOITULOSTI';
}

function getLabelGenericDefault(){
    return 'OTROS';
}

function saveItemStorage(name , value){
    localStorage.setItem(name, value)    
}

function getItemStorage(name){    
    return localStorage.getItem(name);
}

function getCodeRole(){
    var _code_role = localStorage.getItem('_code_role')
    if(_code_role != null && _code_role != undefined && _code_role != 'undefined'){
        if(_code_role.length > 0){
            return _code_role
        }
    }
    return '';
}

function getIdBusiness(defaultvalue= ""){
    var idBusiness = localStorage.getItem('_idBusiness')
    if(idBusiness != null && idBusiness != undefined && idBusiness != 'undefined'){
        if(idBusiness.length > 0){
            return idBusiness
        }
    }
    return defaultvalue;
}

function getBusinessName(){
    var labelBusiness = localStorage.getItem('_businessName')
    if(labelBusiness != null && labelBusiness != undefined && labelBusiness != 'undefined'){
        if(labelBusiness.length > 0){
            return labelBusiness
        }
    }
    return '';
}

function formatMoney(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};



module.exports = {
    getIdPerson,
    getIdUser,
    getToken,
    changeLanguage,
    getMessage,
    getRfcGeneric,
    getRfcCurrent,
    storeMessages,
    saveItemStorage,
    getItemStorage,
    getDataProduct,
    getCodeRole,
    getLabelGenericDefault,
    getIdBusiness,
    getBusinessName,
    formatMoney
}
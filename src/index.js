const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const session = require('express-session');
const path = require('path');
const bodyParser = require('body-parser');
const Config = require('./config');

const app = express();
app.set('port',Config.PORT);

app.set('views', path.join(__dirname, 'views'))


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


app.use(express.static(__dirname + '/public'))

//server listening
const server = app.listen(app.get('port'), () =>{
    console.log("server runing in: "+ app.get('port'));
});
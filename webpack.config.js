const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    entry: './src/app/index.js',
    output: {
        path: __dirname + '/src/public/js',
        filename: 'bundle.js'
    },
    module:     {
        rules:[
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                  loaders: {
                    'scss': 'vue-style-loader!css-loader!sass-loader'
                  }
                  // other vue-loader options go here
                }
              },
              {
                test: /\.css$/,
                loader: 'vue-style-loader!css-loader'
              },
              {
                test: /\.less$/,
                use: [
                  'vue-style-loader',
                  'css-loader',
                  {
                    loader: 'less-loader',
                  }
                ]
              },
              {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
              },
              {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]?[hash]'
                }
              }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};